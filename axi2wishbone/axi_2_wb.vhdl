----
-- Copyright (c) 2016, Sven A. Huerlimann <sh@sighup.ch>
-- All rights reserved.
-- 
-- Redistribution and use in source and binary forms, with or without 
-- modification, are permitted provided that the following conditions are met:
-- 
-- 1. Redistributions of source code must retain the above copyright notice, 
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- 3. Neither the name of the copyright holder nor the names of its contributors
-- may be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
----

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity axi_2_wb is
	generic(
		-- AXI Parameters from template
		C_S_AXI_DATA_WIDTH : integer := 32;
		C_S_AXI_ADDR_WIDTH : integer := 32
	);
	port(
		-- axi slave interface
		S_AXI_ACLK    : in  std_logic;
		S_AXI_ARESETN : in  std_logic;
		S_AXI_AWADDR  : in  std_logic_vector(C_S_AXI_ADDR_WIDTH - 1 downto 0);
		S_AXI_AWPROT  : in  std_logic_vector(2 downto 0);
		S_AXI_AWVALID : in  std_logic;
		S_AXI_AWREADY : out std_logic;
		S_AXI_WDATA   : in  std_logic_vector(C_S_AXI_DATA_WIDTH - 1 downto 0);
		S_AXI_WSTRB   : in  std_logic_vector((C_S_AXI_DATA_WIDTH / 8) - 1 downto 0);
		S_AXI_WVALID  : in  std_logic;
		S_AXI_WREADY  : out std_logic;
		S_AXI_BRESP   : out std_logic_vector(1 downto 0);
		S_AXI_BVALID  : out std_logic;
		S_AXI_BREADY  : in  std_logic;
		S_AXI_ARADDR  : in  std_logic_vector(C_S_AXI_ADDR_WIDTH - 1 downto 0);
		S_AXI_ARPROT  : in  std_logic_vector(2 downto 0);
		S_AXI_ARVALID : in  std_logic;
		S_AXI_ARREADY : out std_logic;
		S_AXI_RDATA   : out std_logic_vector(C_S_AXI_DATA_WIDTH - 1 downto 0);
		S_AXI_RRESP   : out std_logic_vector(1 downto 0);
		S_AXI_RVALID  : out std_logic;
		S_AXI_RREADY  : in  std_logic;

		-- wishbone master interface
		wbm_adr_o     : out std_logic_vector(31 downto 0); -- Address
		wbm_dat_o     : out std_logic_vector(31 downto 0); -- Data out
		wbm_dat_i     : in  std_logic_vector(31 downto 0); -- Data in
		wbm_cyc_o     : out std_logic;  -- Cycle
		wbm_sel_o     : out std_logic_vector(3 downto 0); -- Byte select
		wbm_stb_o     : out std_logic;  -- Strobe
		wbm_we_o      : out std_logic;  -- Write
		wbm_ack_i     : in  std_logic;  -- Acknowledge
		wbm_stall_i   : in  std_logic   -- Stall
	);
end entity axi_2_wb;

architecture RTL of axi_2_wb is
	type axi_proto_read_state_t is (IDLE, READ, WRITE, WRITE2, RRESP, WRESP, RESP2);

	type axi_2_wb_reg_t is record
		state : axi_proto_read_state_t;

		-- AXI
		arready : std_logic;
		awready : std_logic;

		rvalid : std_logic;
		wready : std_logic;

		bvalid : std_logic;
		bresp  : std_logic_vector(1 downto 0);

		rresp : std_logic_vector(1 downto 0);

		rdata : std_logic_vector(31 downto 0);

		-- wishbone
		wb_adr   : std_logic_vector(31 downto 0);
		wb_dat_o : std_logic_vector(31 downto 0);

		wb_we  : std_logic;
		wb_stb : std_logic;
		wb_cyc : std_logic;
	end record;

	signal r, rin : axi_2_wb_reg_t;
begin
	wbm_sel_o <= "1111";

	fsm_comb : process(S_AXI_ARESETN, r, S_AXI_ARVALID, S_AXI_ARADDR, S_AXI_AWADDR, S_AXI_WVALID, S_AXI_BREADY, wbm_ack_i)
		variable v : axi_2_wb_reg_t;
	begin
		v := r;

		v.bvalid := '0';

		v.rresp := "00";
		v.bresp := "00";

		v.wb_cyc := '0';
		v.wb_stb := '0';


		case r.state is
			when IDLE =>
				v.rvalid := '0';
				v.wready := '0';
				v.bvalid := '0';

				v.arready := '0';
				v.awready := '0';

				if (S_AXI_ARVALID = '1') then
					v.state   := READ;
					v.arready := '1';

					v.wb_adr := S_AXI_ARADDR;
					v.wb_we  := '0';
					v.wb_stb := '1';
					v.wb_cyc := '1';

				elsif (S_AXI_AWVALID = '1') then
					v.state := WRITE;

					v.awready := '1';
					v.wready  := '1';

					v.wb_adr := S_AXI_AWADDR;
					v.wb_we  := '1';
				end if;
			when READ =>
				v.arready := '0';

				if (wbm_ack_i = '1') then
					v.state := RRESP;

					v.rdata  := wbm_dat_i;
					v.rvalid := '1';

					v.rresp  := "00";
					v.wb_cyc := '0';
					v.wb_stb := '0';
				else
					v.wb_stb := '1';
					v.wb_cyc := '1';
				end if;
			when WRITE =>
				v.wb_stb := '1';
				v.wb_cyc := '1';

				v.awready := '0';

				if (S_AXI_WVALID = '1') then
					v.state    := WRITE2;
					v.wready   := '0';
					
					v.wb_dat_o := S_AXI_WDATA;
				end if;
			when WRITE2 =>
				if (wbm_ack_i = '1') then
					v.state := WRESP;

					v.bvalid := '1';
					v.bresp  := "00";
				else
					v.wb_stb := '1';
					v.wb_cyc := '1';
				end if;
			when WRESP =>
				if (S_AXI_BREADY = '1') then
					v.state := RESP2;
				end if;
			when RRESP =>
				if (S_AXI_RREADY = '1') then
					v.state  := IDLE;
					v.rvalid := '0';
				end if;
			when RESP2 =>
				v.state  := IDLE;
				v.bvalid := '0';
			when others =>
				v.state := IDLE;
		end case;

		if (S_AXI_ARESETN = '0') then
			v.state  := IDLE;
			v.bvalid := '0';
		end if;

		rin <= v;

		-- drive outputs
		S_AXI_ARREADY <= r.arready;
		S_AXI_AWREADY <= r.awready;

		S_AXI_RVALID <= r.rvalid;
		S_AXI_WREADY <= r.wready;

		S_AXI_BVALID <= r.bvalid;
		S_AXI_BRESP  <= r.bresp;

		S_AXI_RDATA <= r.rdata;

		wbm_adr_o <= r.wb_adr;
		wbm_dat_o <= r.wb_dat_o;

		wbm_we_o  <= r.wb_we;
		wbm_cyc_o <= r.wb_cyc;
		wbm_stb_o <= r.wb_stb;

	end process;

	fsm_seq : process(S_AXI_ACLK)
	begin
		if rising_edge(S_AXI_ACLK) then
			r <= rin;
		end if;
	end process;

end architecture RTL;
